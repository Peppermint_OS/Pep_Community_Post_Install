#!/usr/bin/env bash
# original script created 
# by ManyRoads https://eirenicon.org
# update & run hblock 
# hblock must be installed for this to work

pkill hblock
hblock -S none -D none
hblock
