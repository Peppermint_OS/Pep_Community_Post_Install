#!/bin/sh
# This shell script is PUBLIC DOMAIN. You may do whatever you want with it.
# In the commands below 13 is the variable identified with "xinput list"
# Script by ManyRoads https://eirenicon.org

TOGGLE=$HOME/.toggle_touchpad

if [ ! -e $TOGGLE ]; then
    touch $TOGGLE
    xinput disable 13
    notify-send -u normal -i mouse --icon=/usr/share/icons/HighContrast/256x256/status/touchpad-disabled.png "Trackpad disabled"
else
    rm $TOGGLE
    xinput enable 13
    notify-send -u normal -i mouse --icon=/usr/share/icons/HighContrast/256x256/devices/input-touchpad.png "Trackpad enabled"
fi


