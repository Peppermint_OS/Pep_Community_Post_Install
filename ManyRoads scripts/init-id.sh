#! /bin/bash
# script by ManyRoads https://eirenicon.org
# Both approaches work for me, I hope one works for you.
#
# NOTE: My first seemingly agnostic approach.
# ps --no-headers -o comm 1
#
# an approach specifically tuned to MX Linux & PeppermintOS
# thanks for all the help on the MX Forums
# @ceeslans @oops @AVLinux @siamhie 
#

if [[ `/sbin/init --version` =~ upstart ]]; then echo upstart;
elif [[ `systemctl` =~ -\.mount ]]; then echo systemd;
elif [[ -f /etc/init.d/cron && ! -h /etc/init.d/cron ]]; then echo sysv-init;
else echo cannot tell; fi
systemd
