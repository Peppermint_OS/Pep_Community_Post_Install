#! /bin/bash
# this script utilizes nala (which needs to installed)
# you may modify this to use apt instead of nala
# simply change "nala" to "apt"
# script built by ManyRoads https://eirenicon.org

sudo nala autoclean && /
sudo nala autoremove && /
sudo nala update && /
sudo nala upgrade && /
sudo update-grub
