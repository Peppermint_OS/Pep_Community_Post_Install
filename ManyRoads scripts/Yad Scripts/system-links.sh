#!/bin/bash
#System Maintenance - Tools
# script by ManyRoads https://eirenicon.org

yad --title "System Tools" --form --width=325 --height=200 --posx=850 --posy=200 --text="<b><u>System Maintenance Tools</u></b>" --window-icon=utilities-terminal --image "calligraplan" --image-on-top \
--field="<b>aptitude</b>":fbtn "st -e aptitude" \
--field="<b>Bleachbit</b>":fbtn "bleachbit" \
--field="<b>Clean, Update, Upgrade</b>":fbtn "st -e /home/mark/.local/share/scripts/clean-update.sh" \
--field="<b>Grsync</b>":fbtn "grsync" \
--field="<b>Grub Customizer</b>":fbtn "grub-customizer" \
--field="<b>Network Manager TUI</b>":fbtn "st -e nmtui" \
--field="<b>Print Manager Settings</b>":fbtn "system-config-printer" \
--field="<b>Restart Network</b>":fbtn "systemctl restart NetworkManager" \
--field="<b>Sound Control</b>":fbtn "pavucontrol" \
--field="<b>Synaptic</b>":fbtn "synaptic-pkexec" \
--field="<b>Topgrade (Upgrade)</b>":fbtn "st -e /home/mark/.local/bin/topgrade/topgrade.sh" \
--field="<b>USB Image Writer</b>":fbtn "./Dropbox/appimages/Etcher.AppImage" \
--button=Exit:1
