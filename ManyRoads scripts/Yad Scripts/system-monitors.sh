#!/bin/bash
# system monitors
# script by ManyRoads https://eirenicon.org

yad --center --borders=1 --on-top --sticky --single-click --title="System Monitors" --height=80 --text="<b><u>System Monitoring Tools</u></b>" --window-icon=utilities-terminal --image "calligraplan" --image-on-top\
	--buttons-layout=center \
	--window-icon="gtk-quit: exit" \
	--button="Task Management!gtk-preferences:lxtask" \
	--button="Wavemon!network:st -e wavemon -i wlp2s0" \
	--button="BpyTop!system:st -e bpytop" \
	--button="atop!system-run:st -e atop" \
	--button="htop!cpu:st -e htop" \
	--button="nmon!search:st -e nmon" \
	--button="stacer!memory:stacer" \
	--button=Cancel:0 \
