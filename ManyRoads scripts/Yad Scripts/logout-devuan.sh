#! /bin/bash
# script by ManyRoads https://eirenicon.org

yad --center --borders=1 --on-top --sticky --single-click --title='Logout Options' \
	--buttons-layout=center \
	--window-icon="gtk-quit: exit" \
	--button="Logout!system-log-out:kill -9 -1" \
	--button="Reboot!system-reboot:loginctl reboot" \
	--button="Shutdown!system-shutdown:loginctl poweroff" \
	--button=Cancel:0 \
#	--button="Logout!system-log-out:kill -9 -1" \
#	--button="Reboot!system-reboot:systemctl reboot" \
#	--button="Shutdown!system-shutdown:systemctl poweroff" \
#	--button="Suspend!system-suspend:systemctl suspend" \
#	--button=Cancel:0 \

